﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class TrackInfo
    {
        public RaceTrack raceTrack = null;
        ShortestLanes shortestLanes = null;

        public Dictionary<int, double> runwayAhead = new Dictionary<int, double>();
        public int maxRunwayPiece = 0;

        public TrackInfo(RaceTrack raceTrack)
        {
            this.raceTrack = raceTrack;

            shortestLanes = new ShortestLanes(this);
            //BotLog.LogTrackPieces(this);
            CalcAllRunways();
            //shortestLanes.LogDists();
            //ConsoleMonitor.Log("max: {0}", maxRunwayPiece);
        }

        void CalcAllRunways()
        {
            for (int i = 0; i < raceTrack.pieces.Count; i++)
                CalcRunway(i);

            double max = double.MinValue;
            foreach (var pair in runwayAhead)
            {
                if (pair.Value >= max)
                {
                    max = pair.Value;
                    maxRunwayPiece = pair.Key;
                }
            }
        }

        void CalcRunway(int piece)
        {
            double len = 0;

            if (raceTrack.pieces[piece].radius == 0)
            {
                len = raceTrack.pieces[piece].length;
                int p = NextPiece(piece);

                while ((p != piece) && (raceTrack.pieces[p].radius == 0))
                {
                    len += raceTrack.pieces[p].length;
                    p = NextPiece(p);
                }
            }

            //ConsoleMonitor.Log("Piece: {0}, Runway: {1:0.000}", piece, len);
            runwayAhead.Add(piece, len);
        }

        public string PreferredSwitch(int trackPiece, int lane)
        {
            int switchPiece = NextPiece(trackPiece);
            if (!raceTrack.pieces[switchPiece].@switch) return null;

            int after = NextPiece(switchPiece);

            // ostanemo na istom laneu
            double current = GetPieceDriveLength(switchPiece, lane, lane) + shortestLanes.GetPathLen(after, lane);

            double left = double.MaxValue, right = double.MaxValue;
            if (lane - 1 >= 0) left = GetPieceDriveLength(switchPiece, lane, lane - 1) + shortestLanes.GetPathLen(after, lane - 1);
            if (lane + 1 < raceTrack.lanes.Count) right = GetPieceDriveLength(switchPiece, lane, lane + 1) + shortestLanes.GetPathLen(after, lane + 1);

            //ConsoleMonitor.Log("Left: {0:0.000}, Right: {1:0.000}, Current: {2:0.000}", left, right, current);

            string minDir = null;
            if (left < current) { current = left; minDir = "Left"; }
            if (right < current) { current = right; minDir = "Right"; }
            return minDir;
        }


        public double GetLaneRadius(PiecePosition pos)
        {
            return GetLaneRadius(pos.pieceIndex, pos.inPieceDistance, pos.lane.startLaneIndex, pos.lane.endLaneIndex);
        }


        // bullshit? uvijek se gleda startlane ?!?!
        public double GetLaneRadius(int trackPiece, double position, int startLane, int endLane)
        {
            double l1 = GetLaneRadius(trackPiece, startLane);
            return l1;
            double l2 = GetLaneRadius(trackPiece, endLane);

            double len = GetPieceDriveLength(trackPiece, startLane, endLane);

            double part1 = position / len;
            double part2 = 1 - part1;
            return part1 * l1 + part2 * l2;
        }

        // ovaj private dohvaca samo jedan lane
        private double GetLaneRadius(int trackPiece, int lane)
        {
            // ugh...lijevi lane je na desnim zavojima uvijek vanjski?
            TrackPiece piece = raceTrack.pieces[trackPiece];
            double radius = piece.radius;
            if (radius == 0) return 0;
            double laneDist = raceTrack.lanes[lane].distanceFromCenter;

            if (piece.angle > 0) laneDist = -laneDist;
            radius += laneDist;

            return radius;
        }

        public double GetLaneLength(int pieceIndex, int lane)
        {
            TrackPiece piece = raceTrack.pieces[pieceIndex];
            if (piece.length != 0)
            {
                return piece.length;
            }
            else
            {
                double fullCircle = 2f * GetLaneRadius(pieceIndex, lane) * Math.PI;
                double len = fullCircle * (Math.Abs(piece.angle) / 360);
                return len;
            }
        }

        public double GetPieceDriveLength(int piece, int startLane, int endLane)
        {
            PiecePosition pos = new PiecePosition();
            pos.pieceIndex = piece;
            pos.lane = new Lane() { startLaneIndex = startLane, endLaneIndex = endLane };
            return GetPieceDriveLength(pos);
        }

        // vraca udaljenost pieceaPos, uzimajuci u obzir i krivine i switchanje
        public double GetPieceDriveLength(PiecePosition pos)
        {
            double startLaneLength = GetLaneLength(pos.pieceIndex, pos.lane.startLaneIndex);
            if (pos.lane.startLaneIndex == pos.lane.endLaneIndex)
            {
                return startLaneLength;
            }
            else
            {
                // ovo ne djeluje bas najbolje...za ravni dio pogadja do 3. decimale udaljenost, ali i dalje nije skroz tocno
                // za switchere na zavojima fulava...opcenito ne znam kak se ponasa na switcherima drugacijih duljina i na ravnom
                // jer su u testu svi duljine 100
                double endLaneLength = GetLaneLength(pos.pieceIndex, pos.lane.endLaneIndex);
                double laneDist = Math.Abs(raceTrack.lanes[pos.lane.startLaneIndex].distanceFromCenter - raceTrack.lanes[pos.lane.endLaneIndex].distanceFromCenter);
                double effect = (startLaneLength + endLaneLength) / 2 - 4;
                return Math.Sqrt(effect * effect + laneDist * laneDist) + 4;
            }
        }

        public double Distance(PiecePosition pos1, PiecePosition pos2)
        {
            if (pos1.pieceIndex == pos2.pieceIndex)
            {
                return pos2.inPieceDistance - pos1.inPieceDistance;
            }
            else
            {
                double part1 = GetPieceDriveLength(pos1) - pos1.inPieceDistance;
                double part2 = pos2.inPieceDistance;
                return part1 + part2;
            }
        }

        public int NextPiece(int pieceIndex)
        {
            pieceIndex++;
            if (pieceIndex >= raceTrack.pieces.Count) pieceIndex = 0;
            return pieceIndex;
        }

        // PAZI! jesu li laneovi poredani s lijeva na desno? laneovi imaju svoj neki index, mozda poopciti da se to koristi svuda
        // ovo cudno radi...moras gledati JEDAN UNAPRIJED PIECE, jer kad udjes u njega vec je gotovo  i kasnis sa switchem
        
        /*
        public string PreferredSwitch(int pieceIndex, int lane)
        {
            pieceIndex = NextPiece(pieceIndex);
            if (!raceTrack.pieces[pieceIndex].@switch) return "";

            int leftLane = lane - 1; double leftDist = double.MaxValue;
            int rightLane = lane + 1; double rightDist = double.MaxValue;

            double dist = TotalLengthBeforeNextSwitch(pieceIndex, lane);
            if (leftLane >= 0) leftDist = TotalLengthBeforeNextSwitch(pieceIndex, leftLane);
            if (rightLane < raceTrack.lanes.Count) rightDist = TotalLengthBeforeNextSwitch(pieceIndex, rightLane);

            if (leftDist < dist) return "Left";
            if (rightDist < dist) return "Right";

            return "";
        }

        // racuna zbroj lane duljina za trackove NAKON nevedenog (navedeni = switch) i PRIJE iduceg switcha
        public double TotalLengthBeforeNextSwitch(int pieceIndex, int lane)
        {
            double dist = 0;

            int ndx = NextPiece(pieceIndex);
            while ((ndx != pieceIndex) && (!raceTrack.pieces[ndx].@switch))
            {
                dist += GetLaneLength(ndx, lane);
                ndx = NextPiece(ndx);
            }

            return dist;
        }
        */
    }
}
