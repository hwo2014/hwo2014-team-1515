﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class CarTickData
    {
        public int gameTick;
        public CarPositionsData posData;
        
        // ovo su izvedene vrijednosti i imat ce ih samo data za nas auto...mozda ovome nije mjesto ovdje
        // gdje drugdje to staviti?
        public double requestedThrottle = -1;
        public double throttle = 0;

        public CarTickData(int gameTick, CarPositionsData posData)
        {
            this.gameTick = gameTick;
            this.posData = posData;
        }
    }
}
