﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class ShortestLanes
    {
        TrackInfo trackInfo;
        RaceTrack raceTrack;
        int lanes;

        List<List<double>> paths = new List<List<double>>();

        public ShortestLanes(TrackInfo trackInfo)
        {
            this.trackInfo = trackInfo;
            raceTrack = trackInfo.raceTrack;
            lanes = raceTrack.lanes.Count;

            InitPaths();
            FindDists();
        }

        public void LogDists()
        {
            for (int i = 0; i < paths.Count; i++)
            {
                string log = string.Format("Piece: {0}  ==> ", i);
                for (int l = 0; l < lanes; l++)
                {
                    log += string.Format("{0,7:0.000}, ", paths[i][l]);
                }
                ConsoleMonitor.Log(log);
            }
        }

        void InitPaths()
        {
            for (int i = 0; i < raceTrack.pieces.Count; i++)
            {
                List<double> laneDist = new List<double>();
                for (int l = 0; l < lanes; l++)
                {
                    laneDist.Add(0);
                }
                paths.Add(laneDist);
            }
        }

        public double GetPathLen(int piece, int lane)
        {
            return paths[piece][lane];
        }

        void FindDists()
        {
            int last = raceTrack.pieces.Count - 1;

            for (int piece = last; piece >= 0; piece--)
            {
                for (int lane = 0; lane < lanes; lane++)
                {
                    paths[piece][lane] = trackInfo.GetLaneLength(piece, lane);
                    if (piece < last)
                    {
                        if (!raceTrack.pieces[piece].@switch)
                        {
                            paths[piece][lane] += paths[piece + 1][lane];
                        }
                        else
                        {
                            double leftOption = GetOptionLen(piece, lane, lane - 1);
                            double rightOption = GetOptionLen(piece, lane, lane + 1);
                            double current = paths[piece][lane] + paths[piece + 1][lane];

                            //ConsoleMonitor.Log("Piece {0}, Lane {1}, Current: {2:0.000}, LeftOption: {3:0.000}, RightOption: {4:0.000}", piece, lane, current, leftOption, rightOption);

                            if (leftOption < current) current = leftOption;
                            if (rightOption < current) current = rightOption;

                            paths[piece][lane] = current;
                        }
                    }
                }
            }
        }

        double GetOptionLen(int piece, int laneStart, int laneEnd)
        {
            if ((laneEnd < 0) || (laneEnd >= lanes)) return 999999;

            double diag = trackInfo.GetPieceDriveLength(piece, laneStart, laneEnd);

            return paths[piece + 1][laneEnd] + diag;
        }

    }
}
