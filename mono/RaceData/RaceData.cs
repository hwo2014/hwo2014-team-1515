﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class RaceData
    {
        // ovdje cemo pretpostaviti da tickovi stizu jedan iza drugoga po jedan, pa koristimo listu koja je history tickova
        // alternativa je Dictionary<int, CarTickData> ali mi izgleda kao prevelika komplikacija i usporenje dosta veliko
        Dictionary<string, List<CarTickData>> allCars = new Dictionary<string, List<CarTickData>>();

        public RaceData()
        {
        }

        public void RegisterCarPositions(CarPositions carPos)
        {
            foreach (CarPositionsData cpd in carPos.data)
            {
                CarTickData newTick = new CarTickData(carPos.gameTick, cpd);

                if (!allCars.ContainsKey(cpd.id.color)) allCars.Add(cpd.id.color, new List<CarTickData>());

                // pracenje trazenog i aktivnog throttlea
                double activeThrottle = 0;
                if (allCars[cpd.id.color].Count > 0)
                {
                    CarTickData last = allCars[cpd.id.color].Last();
                    if (last.requestedThrottle != -1) activeThrottle = last.requestedThrottle;
                    else activeThrottle = last.throttle;
                }
                newTick.throttle = activeThrottle;

                allCars[cpd.id.color].Add(newTick);
                //ConsoleMonitor.Log("pos data has {0} elements at tick {1}", allCars.First().Value.Count, carPos.gameTick);
            }
        }

        public List<CarTickData> GetCarData(string carColor)
        {
            if (allCars.ContainsKey(carColor)) return allCars[carColor];
            else return null;
        }
    }
}
