﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    partial class PhysicsModel
    {
        public static double Rad(double angle)
        {
            return angle * Math.PI / 180.0f;
        }

        public static double Deg(double angle)
        {
            return angle * 180.0f / Math.PI;
        }

    }
}
