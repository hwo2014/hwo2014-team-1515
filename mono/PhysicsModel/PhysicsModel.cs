﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    // ova klasa sadrzi opis fizike i sve konstante...nema baznu klasu + derivacije jer cemo koristiti samo jednu implementaciju fizike
    // bar za sada..ideja je da se fizika ne mijenja i da je hardcodirana u funkcijama
    // klasa takodjer sadrzi sve konstante fizike no one su public propertiji i predvidjeni su da druga learning klasa mijenja
    // te fizikalne konstante iz uzoraka koje prikuplja
    partial class PhysicsModel
    {
        public double P = 0.0f;
        public double m = 1.0f;
        public double o = 0.1f;//0.095;//0.1f;

        public double flagDC = 10f; // udaljenost flaga od centra auta
        public double kor = -0.294f;
        public double orc = -3.5f;

        public double CalcAngleSpeed(double da, double dt)
        {
            return da / dt;
        }

        public double CalcAngleAcc(double dav, double dt)
        {
            return dav / dt;
        }

        public double CalcSpeed(double ds, double dt)
        {
            return ds / dt;
        }

        public double CalcAcc(double dv, double dt)
        {
            return dv / dt;
        }

        public double CalcAccInT(double t, double v0, double throttle)
        {
            double F = P * throttle;
            t -= 1;
            double acc = Math.Pow(m - o, t) * (F - o * v0) / Math.Pow(m, t + 1);
            return acc;
        }

        public double CalcSpeedinT(double t, double v0, double throttle)
        {
            double F = P * throttle;
            t -= 1;
            double speed = v0 + Math.Pow(m, -1 - t) * (Math.Pow(m, 1 + t) - Math.Pow(m - o, 1 + t)) * (F - o * v0) / o;
            return speed;
        }

        public double NextSpeed(double v0, double throttle)
        {
            double F = P * throttle;
            double acc = (F - o * v0) / m;
            return v0 + acc;
        }

        // calculate constant o and constant P
        public void CalculateAccConstants(double v0, double v1, double v2)
        {
            o = ((v1 - v2) - (v0 - v1)) / (v1 - v0);
            P = v1 + o * v0 - v0;
        }

        /*
        double sumDriftMin = 0, sumDriftMax = 0;
        double driftSampleCount = 0;
        public void ReportDrift(double radius, double speed, double prevSpeed, double driftAngSpeed, double prevDriftAngSpeed)
        {
            if (radius == 0) return;

            if ((prevDriftAngSpeed == 0) && (driftAngSpeed != 0))
            {
                sumDriftMin += prevSpeed * prevSpeed / radius;
                sumDriftMax += speed * speed / radius;
                driftSampleCount++;

                driftF = (sumDriftMin / driftSampleCount + sumDriftMax / driftSampleCount) / 2.0f;
                ConsoleMonitor.Log("New drift limit found: {0:0.00000}", driftF);
            }
        }
        */
    }
}
