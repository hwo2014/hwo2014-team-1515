﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    partial class PhysicsModel
    {
        public double M = 1333.0f;
        public double K = 1.666f;
        public double C = 133.3f;


        /*
        public double vA = 33.1059f;
        public double vB = 37.97570f;
        public double vC = -15.54820f;
        public double driftF = 0.31f;
        */

        public double vA = 30f;
        public double vB =  60f;
        public double vC = 0f;
        public double driftF = 0.3f;

        /*
        public double vA = 69.1119f;
        public double vB = 57.4865f;
        public double vC = 0.29972f;
        public double rPow = 1f;
        public double vRAdd = 3.2f;

        public double driftF = 0.315f;
        */

        public double NextDriftAngle(double angle, double angSpeed, double Speed, double radius, out double newAngSpeed, double direction)
        {
            angSpeed = Rad(angSpeed);
            double angRad = Rad(angle);
            double negF = -angRad * (K * Speed) - angSpeed * C;

            double FCfg = 0, FFC = 0;

            if (radius > 0)
            {
                double cudni = Math.Sqrt(radius / 90f);
                FCfg = Math.Pow(Speed, 2.0f) / (radius) - driftF;
                if (FCfg > 0)
                {
                    // umjesto vA stavimo cudni2
                    FFC = (vA * FCfg * FCfg + cudni * vB * FCfg + vC) * direction;
                }
            }

            double acc = (negF + FFC) / M;

            newAngSpeed = Deg(angSpeed + acc);
            double newAng = angle + newAngSpeed;
            return newAng;
        }

        public double IsDrifting(double a0, double a1, double a2, double speed1)
        {
            a0 = Rad(a0); a1 = Rad(a1); a2 = Rad(a2);

            double v0 = a1 - a0;
            double v1 = a2 - a1;

            double acc = v1 - v0;

            double F = -a1 * (K * speed1) - v1 * C;
            double missingF = Math.Abs(acc * M - F);

            if (missingF < 0.01f) return 0;
            else return missingF;
        }

        double maxNoDriftF = 0.30f;
        double minDriftF = double.MaxValue;
        int driftThresholdsFound = 0;

        public void RegisterDrift(double speed, double radius)
        {
            double df = speed * speed / radius;
            if (df < minDriftF)
            {
                minDriftF = df;
                //ConsoleMonitor.SetValue("Min Drift F:", string.Format("{0:0.000}", minDriftF));
                //ConsoleMonitor.Log("Min Drift: {0:0.000}", minDriftF);
                CalcNewDriftF();
            }
        }

        public void RegisterNoDrift(double speed, double radius)
        {
            double df = speed * speed / radius;
            if (df > maxNoDriftF)
            {
                maxNoDriftF = df;
                //ConsoleMonitor.SetValue("Max No Drift F:", string.Format("{0:0.000}", maxNoDriftF));
                ConsoleMonitor.Log("Max No Drift: {0:0.000}", maxNoDriftF);
                CalcNewDriftF();
            }
            driftThresholdsFound++;
            ConsoleMonitor.Log("No drift registered!");
        }

        public bool DriftThresholdOk()
        {
            if (driftThresholdsFound > 30) return true;
            else return false;
        }

        void CalcNewDriftF()
        {
            /*
            if ((maxNoDriftF != double.MinValue) && (minDriftF != double.MaxValue))
            {
                driftF = (minDriftF + maxNoDriftF) / 2f;
            }
            */

            if (maxNoDriftF != double.MinValue) driftF = maxNoDriftF;
        }
    }
}
