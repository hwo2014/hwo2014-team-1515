﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class SuperBotA1 : DriveBot
    {
        public static int _throttleIters = 4;
        public static double _driftLimit = 58.5f; // prod vrijednost je 58.5

        public SuperBotA1(PhysicsModel physicsModel)
            : base(physicsModel)
        {
        }

        public override string GetName()
        {
            return "MathFestSux";
        }

        int lastSwitchCheckedPiece = -1;

        void CalculateAccConstants()
        {
            double v0 = carComputer.Speed(3);
            double v1 = carComputer.Speed(4);
            double v2 = carComputer.Speed(5);
            physicsModel.CalculateAccConstants(v0, v1, v2);
            ConsoleMonitor.Log("AirDrag (o) == {0:0.00000}, EnginePower (P) == {1:0.00000}", physicsModel.o, physicsModel.P);
            ConsoleMonitor.Redraw();
        }

        double CheckDriftThreshold()
        {
            PiecePosition pos = carComputer.ActualPositionData().piecePosition;

            // brejka se u switchu
            if (pos.lane.startLaneIndex != pos.lane.endLaneIndex) return -1;

            int tick = carComputer.ActualTick();

            CarTickData t0 = carComputer.TickData(tick - 2);
            CarTickData t1 = carComputer.TickData(tick - 1);
            CarTickData t2 = carComputer.TickData(tick);

            double radius1 = trackInfo.GetLaneRadius(t1.posData.piecePosition);
            double radius0 = trackInfo.GetLaneRadius(t0.posData.piecePosition);

            double speed1 = carComputer.Speed(tick - 1);
            double isDrifting = physicsModel.IsDrifting(t0.posData.angle, t1.posData.angle, t2.posData.angle, speed1);

            if ((radius1 > 0) && (radius0 == radius1))
            {
                if (isDrifting > 0f)
                {
                    physicsModel.RegisterDrift(speed1, radius1);
                }
                else
                {
                    double speed2 = carComputer.ActualSpeed();
                    physicsModel.RegisterNoDrift(speed1, radius1);
                }
            }

            if ((!physicsModel.DriftThresholdOk()) && (tick < 200))
            {
                if (radius1 > 0)
                {
                    if (isDrifting > 0)
                    {
                        ConsoleMonitor.Log("Drifting: {0:0.000}", isDrifting);
                        return 0.0f;
                    }
                    else return 1.0f;
                }
            }
            return -1;
        }

        string CheckSwitch()
        {
            PiecePosition pos = carComputer.ActualPositionData().piecePosition;
            
            if (pos.pieceIndex != lastSwitchCheckedPiece)
            {
                lastSwitchCheckedPiece = pos.pieceIndex;
                string dir = trackInfo.PreferredSwitch(pos.pieceIndex, pos.lane.endLaneIndex);
                if (!string.IsNullOrEmpty(dir))
                {
                    ConsoleMonitor.Log("Want switch at piece {0} to the {1} !", pos.pieceIndex, dir);
                    return dir;
                }
            }
            return null;
        }

        void ProcessTurbo()
        {
            if (turboInUse != null)
            {
                turboInUse.turboDurationTicks--;
            }
        }

        bool ShouldTurboAnyway()
        {
            if ((turboAvailable == null) || (turboInUse != null) || turboRequested) 
                return false;

            if (carComputer.ActualPositionData().piecePosition.pieceIndex == trackInfo.maxRunwayPiece)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    
        public override SendMsg ProcessMessage(CarPositions carPositions)
        {
            double throttle = -1; // -1 == unset

            if (!raceStarted) return null;

            raceData.RegisterCarPositions(carPositions);

            if (crashed) return new Ping();
            
            ProcessTurbo();

            int tick = carPositions.gameTick;

            if (tick == 5) CalculateAccConstants();

            // gledanje min drifta se brejka kad je auto u switchu
            if (tick > 5) throttle = CheckDriftThreshold();

            string switchDir = CheckSwitch();
            if (!string.IsNullOrEmpty(switchDir)) 
                return new SwitchLane(switchDir);

            if (SafeToTurbo() && !turboRequested)
            {
                ConsoleMonitor.Log("****** SAFE !!! ACTIVATING!!! ********");
                turboRequested = true;
                return new Turbo();
            }

            if (ShouldTurboAnyway())
            {
                ConsoleMonitor.Log("****** TURBO ANYWAY !!!! **************");
                turboRequested = true;
                return new Turbo();
            }
            
            if (throttle == -1) throttle = FindOptimalThrottle();

            raceData.GetCarData(carColor).Last().requestedThrottle = throttle;

            return new Throttle(throttle);
        }

        void Test(double throttle)
        {
            Simulator sim = new Simulator(this);
            sim.AdvanceTick(1.0f);
            for (int i = 0; i < 50; i++)
            {
                BotLog.LogSimulator(sim);
                sim.AdvanceTick(throttle);
            }
        }

        double FindOptimalThrottle()
        {
            double sol = 1.0f;
            double lastGood = 0f;

            for (int i = 0; i < _throttleIters; i++)
            {
                if (CanCrash(sol))
                {
                    sol = sol / 2f;
                }
                else
                {
                    lastGood = sol;
                    sol = (sol + 1.0f) / 2f;
                }
            }

            return lastGood;
        }

        bool DontCheck(int startPiece, int startLap, int currentPiece)
        {
            int laps = gameInit.data.race.raceSession.laps;
            if ((laps > 0) && (currentPiece < startPiece) && (startLap == laps - 1))
            {
                //ConsoleMonitor.Log("Abort check at lap {0}, startpiece {1}, endpiece {2}", startLap, startPiece, currentPiece);
                return true;
            }
            else return false;
        }

        bool SafeToTurbo()
        {
            if (turboAvailable == null) return false;
            if (turboInUse != null) return false;

            Simulator sim = new Simulator(this);

            PiecePosition pos = carComputer.ActualPositionData().piecePosition;

            sim.AdvanceTick(carComputer.ActualThrottle());
            sim.AdvanceTick(1.0f);
            sim.SetTurbo(turboAvailable.turboFactor, turboAvailable.turboDurationTicks);
            while (sim.speed > 0.1f)
            {
                if (DontCheck(pos.pieceIndex, pos.lap, sim.trackTraverse.pieceIndex)) break;
                //BotLog.LogSimulator(sim);
                if (Math.Abs(sim.drift) >= _driftLimit) return false;
                if (sim.turbo.turboDurationTicks >= 0) sim.AdvanceTick(1.0f);
                else sim.AdvanceTick(0.0f);
            }
            return true;
        }

        bool CanCrash(double throttle)
        {
            Simulator sim = new Simulator(this);
            PiecePosition pos = carComputer.ActualPositionData().piecePosition;
            
            sim.AdvanceTick(carComputer.ActualThrottle());
            sim.AdvanceTick(throttle);

            while (sim.speed > 0.1f)
            {
                if (DontCheck(pos.pieceIndex, pos.lap, sim.trackTraverse.pieceIndex)) break;
                if (Math.Abs(sim.drift) >= _driftLimit)
                {
                    //ConsoleMonitor.Log("Will crash at tick: {0}, angle: {1:0.000}", sim.tick, sim.drift);
                    return true;
                }
                sim.AdvanceTick(0.0f);
            }
            return false;
        }

    }
}
