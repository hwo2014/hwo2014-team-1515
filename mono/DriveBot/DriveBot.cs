﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace SuperCars
{
    // bazna klasa za bot koji vozi...derivirati ovu klasu za alternativne implementacije
    class DriveBot
    {
        public PhysicsModel physicsModel;
        public TrackInfo trackInfo;
        public RaceData raceData = new RaceData();
        public CarComputer carComputer;
        public GameInit gameInit;

        public string carColor = null;

        public bool raceInitialized = false;
        public bool raceStarted = false;

        public bool crashed = false;
        public TurboData turboAvailable = null;
        public TurboData turboInUse = null;
        public bool turboRequested = false;

        public DriveBot(PhysicsModel physicsModel)
        {
            this.physicsModel = physicsModel;
            carComputer = new CarComputer(this);
        }

        public virtual string GetName()
        {
            return "default bot";
        }

        public virtual SendMsg ProcessMessage(CarPositions carPositions)
        {
            raceData.RegisterCarPositions(carPositions);
            double throttle = 1.0f;

            raceData.GetCarData(carColor).Last().requestedThrottle = throttle;

            return new Throttle(throttle);
        }

        public virtual SendMsg ProcessMessage(GameInit gameInit)
        {
            raceInitialized = true;
            trackInfo = new TrackInfo(gameInit.data.race.track);
            this.gameInit = gameInit;
            return new Ping();
        }

        public virtual SendMsg ProcessMessage(YourCar yourCar)
        {
            carColor = yourCar.data.color;
            return null;
        }

        public virtual SendMsg ProcessMessage(GameStart gameStart)
        {
            raceStarted = true;
            return new Throttle(1.0f);
        }

        public virtual SendMsg ProcessMessage(TurboAvailable turboAvailable)
        {
            if (!crashed)
            {
                this.turboAvailable = turboAvailable.data;
                ConsoleMonitor.Log("Received turbo with factor {0:0.00} and duration {1}", this.turboAvailable.turboFactor, this.turboAvailable.turboDurationTicks);
            }
            return null;
        }

        public virtual SendMsg ProcessMessage(CarCrash carCrash)
        {
            if (carCrash.data.color == carColor)
            {
                crashed = true;
                turboAvailable = null;
                turboInUse = null;
                turboRequested = false;
            }
            return null;
        }

        public virtual SendMsg ProcessMessage(CarSpawn carSpawn)
        {
            if (carSpawn.data.color == carColor)
            {
                crashed = false;
            }

            return null;
        }

        public virtual SendMsg ProcessMessage(TurboStart turboStart)
        {
            if (turboStart.data.color == carColor)
            {
                turboInUse = turboAvailable;
                turboAvailable = null;
                turboRequested = false;
            }
            return null;
        }

        public virtual SendMsg ProcessMessage(TurboEnd turboEnd)
        {
            if (turboEnd.data.color == carColor)
            {
                turboInUse = null;
            }
            return null;
        }
    }
}
