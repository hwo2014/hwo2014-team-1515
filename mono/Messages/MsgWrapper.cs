﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class MsgWrapper
    {
        public string msgType;
        public Object data;

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }
}
