﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class CarID
    {
        public string name = "";
        public string color = "";
    }

    class PiecePosition
    {
        public int pieceIndex = 0;
        public double inPieceDistance = 0;
        public Lane lane = null;
        public int lap = 0;
    }

    class Lane
    {
        public int startLaneIndex = 0;
        public int endLaneIndex = 0;
    }

    class CarPositionsData
    {
        public CarID id = null;
        public double angle = 0;
        public PiecePosition piecePosition = null;
    }

    class CarPositions
    {
        public List<CarPositionsData> data = null;
        public string gameId = "";
        public int gameTick = 0;

        public CarPositionsData CarByName(string name)
        {
            foreach (CarPositionsData car in data)
            {
                if (car.id.name == name) return car;
            }
            return null;
        }
    }

    class CarDimensions
    {
        public double length = 0;
        public double width = 0;
        public double guideFlagPosition = 0;
    }

    class TrackPiece
    {
        public double length = 0;
        public double radius = 0;
        public double angle = 0;
        public bool @switch = false;
    }

    class TrackLane
    {
        public double distanceFromCenter = 0;
        public int index = 0;
    }

    class StartingPointPosition
    {
        public double x = 0;
        public double y = 0;
    }

    class StartingPoint
    {
        public StartingPointPosition position = null;
        public double angle = 0;
    }

    class RaceTrack
    {
        public string id = null;
        public string name = null;
        public List<TrackPiece> pieces = null;
        public List<TrackLane> lanes = null;
        public StartingPoint startingPoint = null;
    }

    class RaceCar
    {
        public CarID id = null;
        public CarDimensions dimensions = null;
    }

    class RaceSession
    {
        public int laps = 0;
        public int maxLapTimeMs = 0;
        public bool quickRace = false;
        public double durationMs = 0;
    }

    class Race
    {
        public RaceTrack track = null;
        public List<RaceCar> cars = null;
        public RaceSession raceSession = null;
    }

    class GameInitData
    {
        public Race race = null;
    }

    class GameInit
    {
        public GameInitData data = null;
    }

    class YourCar
    {
        public YourCarData data = null;
    }

    class YourCarData
    {
        public string name = null;
        public string color = null;
    }

    class TurboData
    {
        public double turboDurationMilliseconds = 0;
        public int turboDurationTicks = 0;
        public double turboFactor = 0;
    }

    class TurboAvailable
    {
        public TurboData data = null;
    }

    class GameStart
    {
       
    }

    class CarCrash
    {
        public CarID data = null;
    }

    class CarSpawn
    {
        public CarID data = null;
    }

    class TurboStart
    {
        public CarID data = null;
    }

    class TurboEnd
    {
        public CarID data = null;
    }
}
