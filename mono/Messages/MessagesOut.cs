﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace SuperCars
{
    abstract class SendMsg
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
        protected virtual Object MsgData()
        {
            return this;
        }

        public abstract string MsgType();
    }

    class Join : SendMsg
    {
        public string name;
        public string key;
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        public override string MsgType()
        {
            return "join";
        }
    }

    class BotId
    {
        public string name = "";
        public string key = "";
    }

    class JoinRace : SendMsg
    {
        public BotId botId = new BotId();
        public string trackName = "";
        public string password = null;
        public int carCount = 1;

        public JoinRace(string trackName, string botName, string key)
        {
            botId.name = botName;
            botId.key = key;
            this.trackName = trackName;
            //this.password = "123";
            //this.carCount = 4;
        }

        public override string MsgType()
        {
            return "joinRace";
        }
    }

    class Turbo : SendMsg
    {
        public override string MsgType()
        {
            return "turbo";
        }

        protected override object MsgData()
        {
            return "Reverse engineering functions sucks!";
        }
    }

    class Ping : SendMsg
    {
        public override string MsgType()
        {
            return "ping";
        }
    }

    class Throttle : SendMsg
    {
        public double value;

        public Throttle(double value)
        {
            this.value = value;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        public override string MsgType()
        {
            return "throttle";
        }
    }

    class SwitchLane : SendMsg
    {
        public string direction;
        public SwitchLane(string direction)
        {
            this.direction = direction;
        }
        protected override object MsgData()
        {
            return direction;
        }
        public override string MsgType()
        {
            return "switchLane";
        }
    }
}
