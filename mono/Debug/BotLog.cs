﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class BotLog
    {
        public static void LogBotInfoHeaders()
        {
            ConsoleMonitor.Log("Tick, Piece, PieceAngle, PieceRadius, ActualRadius, PieceLength, Position, Lane0, Lane1, Angle, Speed, Acc, Throttle");
        }

        public static void LogBotInfo(DriveBot bot)
        {
            CarPositionsData cpd = bot.carComputer.ActualPositionData();
            if (cpd == null) return;
            PiecePosition pos = cpd.piecePosition;
            int tick = bot.carComputer.ActualTick();
            TrackPiece piece = bot.trackInfo.raceTrack.pieces[pos.pieceIndex];
            double speed = bot.carComputer.ActualSpeed();
            double acc = bot.carComputer.ActualAcc();
            double throttle = bot.carComputer.ActualThrottle();

            double driveLen = bot.trackInfo.GetPieceDriveLength(pos);

            ConsoleMonitor.Log("{0}, {1}, {2:0}, {3:0}, {12:0.0}, {11:0.000}, {4:0.000}, {5}, {6}, {7:0.00000}, {8:0.000}, {9:0.000}, {10:0.000}",
                                    tick, pos.pieceIndex, piece.angle, piece.radius,
                                    pos.inPieceDistance, pos.lane.startLaneIndex, pos.lane.endLaneIndex,
                                    cpd.angle, speed, acc, throttle, driveLen,
                                    bot.trackInfo.GetLaneRadius(pos)
                                    );
        }

        public static void LogTrackPieces(TrackInfo trackInfo)
        {
            for (int i = 0; i < trackInfo.raceTrack.pieces.Count; i++)
            {
                var tp = trackInfo.raceTrack.pieces[i];
                ConsoleMonitor.Log("{0}, {1:0.000}, {2:0.000}, {3:0.000}, {4}", i, tp.length, tp.angle, tp.radius, tp.@switch);
            }
        }

        public static void LogSimulator(Simulator sim)
        {
            ConsoleMonitor.Log("{0}, {1:0.000}, {2:0.000}, {3:0.000}, {4}{5}", sim.tick, sim.speed, sim.drift, sim.trackTraverse.pos, sim.trackTraverse.startLane, sim.trackTraverse.endLane);
        }

        public static void LogCar(DriveBot bot)
        {
            CarDimensions cd = bot.gameInit.data.race.cars[0].dimensions;
            ConsoleMonitor.Log("Car length: {0:0.000}, Car width: {1:0.000} Flag: {2:0.000}", cd.length, cd.width, cd.guideFlagPosition);
        }


        public static void LogAngleForcesHeaders()
        {
            ConsoleMonitor.Log("Tick, PieceA, PieceR, Speed, Angle, Force, AngleAcc, DeltaF");
        }

        static double Rad(double deg)
        {
            return deg * Math.PI / 180;
        }

        public static void LogAngleForces(DriveBot bot)
        {
            CarPositionsData cpd = bot.carComputer.ActualPositionData();
            if (cpd == null) return;
            PiecePosition pos = cpd.piecePosition;
            int tick = bot.carComputer.ActualTick();
            TrackPiece piece = bot.trackInfo.raceTrack.pieces[pos.pieceIndex];
            double speed = bot.carComputer.ActualSpeed();
            double acc = bot.carComputer.ActualAcc();
            double throttle = bot.carComputer.ActualThrottle();

            double driveLen = bot.trackInfo.GetPieceDriveLength(pos);

            double angAcc = bot.carComputer.ActualAngleAcc();
            
            double M = 1333.0f, K = 1.66666f, C = 133.3f;
            double F = -(K * speed) * Rad(cpd.angle) - Rad(bot.carComputer.ActualAngleSpeed()) * C;
            double dF = Rad(angAcc) * M - F;

            ConsoleMonitor.Log("{0}, {1}, {2:0.000}, {3:0.000}, {4:0.000000}, {5:0.000000}, {6:0.00000000}, {7:0.000000000}",
                tick, piece.angle, bot.trackInfo.GetLaneRadius(pos),
                speed, cpd.angle, F, angAcc, dF);
        }

    }
}
