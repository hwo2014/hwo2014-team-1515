﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;

namespace SuperCars
{
    class ConsoleMonitor
    {
        static Dictionary<string, string> values = new Dictionary<string, string>();
        static LinkedList<string> log = new LinkedList<string>();

        static int _columns = 2;
        static int _titleWidth = 18, _valueWidth = 7;
        static int _logCount = 15;
        static string _separator = "  |  ";
        static string _fileName = "";

        public static void Log(string line)
        {
#if TEST
            log.AddFirst(line);
            if (!string.IsNullOrEmpty(_fileName))
            {
                TextWriter tw = File.AppendText(_fileName);
                tw.WriteLine(line);
                tw.Flush(); tw.Close();
            }
#endif 
        }

        public static void Log(string format, params object[] args)
        {
#if TEST
            string line = string.Format(CultureInfo.InvariantCulture, format, args);
            Log(line);
#endif 
        }
         
        public static void SetNewValues(Dictionary<string, string> newValues)
        {
            values = newValues;                
        }

        public static void SetValue(string name, string value)
        {
            if (values.ContainsKey(name)) values[name] = value;
            else values.Add(name, value);
        }

        public static void Redraw()
        {
#if TEST
            Console.Clear();
            PrintValues();
            PrintLog();
#endif
        }

        private static void PrintValues()
        {
            int inCol = 0;
            foreach (var pair in values)
            {
                Console.Write(pair.Key.PadRight(_titleWidth));
                Console.Write(pair.Value.PadLeft(_valueWidth));
                if (++inCol >= _columns)
                {
                    Console.WriteLine();
                    inCol = 0;
                }
                else
                {
                    Console.Write(_separator);
                }
            }
            Console.WriteLine();
            Console.WriteLine();
        }

        private static void PrintLog()
        {
            int count = Math.Min(log.Count, _logCount);
            var part = log.Take(count).ToList();
            part.Reverse();
            foreach (string line in part) Console.WriteLine(line);
        }

        public static void SetFileLog(string fileName)
        {
            _fileName = fileName;
        }
    }
}
