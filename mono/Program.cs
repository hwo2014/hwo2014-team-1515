﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace SuperCars
{
    class Program
    {
        static void TestDriftCalc()
        {
            PhysicsModel pm = new PhysicsModel();
            double speed = 6.0f;
            double ang = PhysicsModel.Deg(0.00351f);
            double angV = PhysicsModel.Deg(0.0037350f);
            double radius = 90f;

            for (int i = 0; i < 10; i++)
            {
                double newV;
                ang = pm.NextDriftAngle(ang, angV, speed, radius, out newV, 1);
                angV = newV;
                ConsoleMonitor.Log("{0}, {1:0.0000}, {2:0.0000}", i, PhysicsModel.Rad(ang), PhysicsModel.Rad(angV));
                ConsoleMonitor.Redraw();
            }
        }

        static void TestPrediction(DriveBot bot)
        {
            Simulator sim = new Simulator(bot);
            int tick = bot.carComputer.ActualTick();
            double throttle = bot.carComputer.ActualThrottle();
            sim.AdvanceTick(throttle);
            sim.AdvanceTick(1.0);

            ConsoleMonitor.Log("Tick, Speed, Drift");
            for (int i = 0; i < 50; i++)
            {
                sim.AdvanceTick(1.0);
                BotLog.LogSimulator(sim);
            }
            ConsoleMonitor.Redraw();
        }

        static void Main(string[] args)
        {
            double maxDrift = 0;
            HWOClient client = new HWOClient(args[0], int.Parse(args[1]), new PhysicsModel());
#if TEST            
            client.JoinRace("usa");
            ConsoleMonitor.SetFileLog("log.txt");
#else
            client.Join();
#endif

            BotLog.LogBotInfoHeaders();

            BotLog.LogAngleForcesHeaders();

            while (client.IsConnected())
            {
                client.ProcessMessages();
                if (client.lastServerMsg != null)
                {
                    if (client.bot.raceInitialized)
                    {
                        int tick = client.bot.carComputer.ActualTick();

                        if (tick == 623)
                        {
                            //TestPrediction(bot);
                        }

                        CarPositionsData cpd = client.bot.carComputer.ActualPositionData();
                        if (cpd != null)
                        {
                            if (Math.Abs(cpd.angle) > maxDrift) maxDrift = Math.Abs(cpd.angle);
                            ConsoleMonitor.SetValue("Tick", client.bot.carComputer.ActualTick().ToString());
                            ConsoleMonitor.SetValue("", "");
                            ConsoleMonitor.SetValue("Engine Power", client.bot.physicsModel.P.ToString("0.0000"));
                            ConsoleMonitor.SetValue("Air Drag", client.bot.physicsModel.o.ToString("0.0000"));
                            ConsoleMonitor.SetValue("Max Drift", maxDrift.ToString("0.0000"));
                            ConsoleMonitor.SetValue("Drift Threshold", client.bot.physicsModel.driftF.ToString("0.0000"));
                            BotLog.LogBotInfo(client.bot);
                        }
                    }
                    
                    ConsoleMonitor.Redraw();
                }
            }
        }
    }
}
