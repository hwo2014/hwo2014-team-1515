﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

namespace SuperCars
{
    // klasa se bavi connectivitijem sa serverom i race workflowom (qualifying race, itd)
    class HWOClient
    {
        public static string _botKey = "W/7oSc8s7qVJ7g";
        TcpClient tcpClient;
        NetworkStream networkStream;
        StreamReader tcpReader;
        StreamWriter tcpWriter;
        bool connected;

        string host;
        int port;
        public DriveBot bot;

        public MsgWrapper lastServerMsg = null;
        public SendMsg lastResponse = null;
        public PhysicsModel physicsModel;

        public HWOClient(string host, int port, PhysicsModel physicsModel)
        {
            this.physicsModel = physicsModel;
            this.host = host; this.port = port; this.bot = new SuperBotA1(physicsModel);

            tcpClient = new TcpClient(host, port);
            networkStream = tcpClient.GetStream();
            tcpReader = new StreamReader(networkStream);
            tcpWriter = new StreamWriter(networkStream);
            tcpWriter.AutoFlush = true;
            connected = true;
        }

        public void ProcessMessages()
        {
            lastServerMsg = null;
            lastResponse = null;

            if (!tcpClient.Connected) connected = false;

            if (networkStream.DataAvailable && connected)
            {
                string msg = tcpReader.ReadLine();

                if (msg != null)
                {
                    string response = ProcessMessage(msg);
                    if (!string.IsNullOrEmpty(response)) tcpWriter.WriteLine(response);
                }
                else
                {
                    connected = false;
                }
            }
            if (tcpReader.EndOfStream) connected = false;
        }

        string ProcessMessage(string message)
        {
            lastServerMsg = JsonConvert.DeserializeObject<MsgWrapper>(message);
            lastResponse = null;
            //ConsoleMonitor.Log(lastServerMsg.msgType);
            Console.WriteLine(DateTime.Now.ToString("G") + " --> " + lastServerMsg.msgType);
            switch (lastServerMsg.msgType)
            {
                case "carPositions":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<CarPositions>(message));
                    break;

                case "yourCar":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<YourCar>(message));
                    break;
                
                case "join":
                    break;

                case "gameInit":
                    GameInit gameInit = JsonConvert.DeserializeObject<GameInit>(message);
                    ConsoleMonitor.Log("GameInit, duration {0}, laps {1}", gameInit.data.race.raceSession.durationMs, gameInit.data.race.raceSession.laps);
                    
                    // ovo znaci da pocinje race
                    if (gameInit.data.race.raceSession.durationMs == 0)
                    {
                        string color = bot.carColor;
                        bot = new SuperBotA1(physicsModel);
                        bot.carColor = color;
                    }

                    lastResponse = bot.ProcessMessage(gameInit);
                    break;

                case "gameEnd":
                    //lastResponse = new Ping();
                    break;

                case "gameStart":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<GameStart>(message));
                    break;

                case "finish":
                    //lastResponse = new Ping();
                    break;

                case "tournamentEnd":
                    connected = false;
                    break;

                case "turboAvailable":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<TurboAvailable>(message));
                    break;

                case "crash":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<CarCrash>(message));
                    if (bot.crashed) ConsoleMonitor.Log("****** CRASHED ! ********");
                    break;

                case "spawn":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<CarSpawn>(message));
                    break;

                case "turboStart":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<TurboStart>(message));
                    ConsoleMonitor.Log("Turbo activated!");
                    break;

                case "turboEnd":
                    lastResponse = bot.ProcessMessage(JsonConvert.DeserializeObject<TurboEnd>(message));
                    ConsoleMonitor.Log("Turbo ended!");
                    break;

                default:
                    ConsoleMonitor.Log(message);
                    break;
            }
            /*
            string log = lastServerMsg.msgType;
            if (lastResponse != null) log += " ==> " + lastResponse.MsgType();
            ConsoleMonitor.Log(log);
            */
            if (lastResponse == null)
            {
                return null;
            }
            else
            {
                //ConsoleMonitor.Log("Response: {0}", lastResponse.MsgType());
                Console.WriteLine(DateTime.Now.ToString("G") + " <-- " + lastResponse.MsgType());
                return lastResponse.ToJson();
            }
        }

        public bool IsConnected()
        {
            return connected;
        }

        public void Join()
        {
            SendMessage(new Join(bot.GetName(), _botKey));
        }

        public void JoinRace(string trackName)
        {
            SendMessage(new JoinRace(trackName, bot.GetName(), _botKey));
        }

        private void SendMessage(SendMsg message)
        {
            tcpWriter.WriteLine(message.ToJson());
        }
    }
}
