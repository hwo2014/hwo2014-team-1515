﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class TrackTraverse
    {
        public TrackInfo trackInfo;
        public int pieceIndex;
        public double pos;
        public int startLane, endLane;
        public string pendingSwitch = null;

        public TrackTraverse(TrackInfo trackInfo, int pieceIndex, double pos, int startLane, int endLane)
        {
            this.trackInfo = trackInfo;
            this.pieceIndex = pieceIndex;
            this.pos = pos;
            this.startLane = startLane;
            this.endLane = endLane;
        }

        public void Switch(string dir)
        {
            pendingSwitch = dir;
        }

        // oprez! funkcija ne dozvoljavanje 1 pomak preko 2 piecea
        public void Move(double ds)
        {
            double len = trackInfo.GetPieceDriveLength(pieceIndex, startLane, endLane);
            double newPos = pos + ds;
            
            // treba li novi piece
            if (newPos >= len)
            {
                pieceIndex++; if (pieceIndex >= trackInfo.raceTrack.pieces.Count) pieceIndex = 0;
                pos = newPos - len;
                startLane = endLane;

                if (!string.IsNullOrEmpty(pendingSwitch))
                {
                    if (pendingSwitch == "Left") endLane--;
                    else if (pendingSwitch == "Right") endLane++;

                    if ((endLane < 0) || (endLane >= trackInfo.raceTrack.lanes.Count))
                    {
                        endLane = startLane;
                        ConsoleMonitor.Log("WARNING: prediction switch to illegal lane!");
                    }

                    pendingSwitch = null;
                }
            }
            else
            {
                pos = newPos;
            }
        }
    }
}
