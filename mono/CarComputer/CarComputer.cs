﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    // ova klasa povezuje podatke o raceu i physics model i racuna razne vrijednosti
    class CarComputer
    {
        PhysicsModel physicsModel;
        DriveBot driveBot;

        public CarComputer(DriveBot driveBot)
        {
            this.driveBot = driveBot;
            physicsModel = driveBot.physicsModel;
        }

        public List<CarTickData> TickData()
        {
            List<CarTickData> result = driveBot.raceData.GetCarData(driveBot.carColor);
            // ako nije jos inicijalizirano, tretiramo kao da je prazan history
            if (result == null) return new List<CarTickData>();
            else return result;
        }

        public CarTickData TickData(int tick)
        {
            List<CarTickData> all = driveBot.raceData.GetCarData(driveBot.carColor);
            if (all.Count < tick) return null;
            else return all[tick - 1];
        }

        public int ActualTick()
        {
            List<CarTickData> tickData = TickData();
            if (tickData.Count == 0) return -1;
            else return tickData.Last().gameTick;
        }

        public double Speed(int tick)
        {
            if (tick < 2) return 0;
            tick--;

            List<CarTickData> tickData = TickData();

            double ds = driveBot.trackInfo.Distance(tickData[tick - 1].posData.piecePosition, tickData[tick].posData.piecePosition);
            return physicsModel.CalcSpeed(ds, tickData[tick].gameTick - tickData[tick - 1].gameTick);
        }

        public double ActualAngle()
        {
            CarTickData last = TickData().LastOrDefault();
            if (last == null) return 0;
            else return last.posData.angle;
        }

        public double AngleSpeed(int tick)
        {
            if (tick < 2) return 0;
            tick--;

            List<CarTickData> tickData = TickData();

            double da = tickData[tick].posData.angle - tickData[tick - 1].posData.angle;
            return physicsModel.CalcAngleSpeed(da, tickData[tick].gameTick - tickData[tick - 1].gameTick);
        }

        public double ActualAngleSpeed()
        {
            int lastTick = TickData().Count;
            return AngleSpeed(lastTick);
        }

        public double ActualAngleAcc()
        {
            List<CarTickData> tickData = TickData();

            int lastTick = tickData.Count;
            if (lastTick < 2) return 0;

            double dav = AngleSpeed(lastTick) - AngleSpeed(lastTick - 1);
            return physicsModel.CalcAngleAcc(dav, tickData[lastTick-1].gameTick - tickData[lastTick - 2].gameTick);
        }

        public double ActualSpeed()
        {
            int lastTick = TickData().Count;
            return Speed(lastTick);
        }

        public double ActualAcc()
        {
            List<CarTickData> tickData = TickData();

            int lastTick = tickData.Count;
            if (lastTick < 2) return 0;
            double dv = Speed(lastTick) - Speed(lastTick - 1);
            return physicsModel.CalcAcc(dv, tickData[lastTick-1].gameTick - tickData[lastTick - 2].gameTick);
        }

        public double ActualThrottle()
        {
            List<CarTickData> tickData = TickData();
            if (tickData.Count == 0) return 0;
            else return tickData.Last().throttle;
        }


        public CarPositionsData ActualPositionData()
        {
            List<CarTickData> tickData = TickData();
            if (tickData.Count == 0) return null;
            else return tickData.Last().posData;
        }
    }
}
