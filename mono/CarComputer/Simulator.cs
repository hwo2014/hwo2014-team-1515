﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperCars
{
    class Simulator
    {
        DriveBot bot;

        public TrackTraverse trackTraverse;
        public double drift, angSpeed, speed;
        public int tick;
        public int lastSwitchCheck = -1;
        public TurboData turbo = null;

        public Simulator(DriveBot bot)
        {
            this.bot = bot;
            InitValues();
        }

        public void SetTurbo(double factor, int duration)
        {
            turbo = new TurboData();
            turbo.turboFactor = factor;
            turbo.turboDurationTicks = duration;
        }

        void InitValues()
        {
            if (bot.turboInUse != null)
            {
                SetTurbo(bot.turboInUse.turboFactor, bot.turboInUse.turboDurationTicks);
            }

            drift = bot.carComputer.ActualAngle();
            angSpeed = bot.carComputer.ActualAngleSpeed();
            speed = bot.carComputer.ActualSpeed();
            tick = bot.carComputer.ActualTick();
            PiecePosition pos = bot.carComputer.ActualPositionData().piecePosition;
            trackTraverse = new TrackTraverse(bot.trackInfo, pos.pieceIndex, pos.inPieceDistance, pos.lane.startLaneIndex, pos.lane.endLaneIndex);
        }

        public void AdvanceTick(double throttle)
        {
            tick++;

            if (trackTraverse.pieceIndex != lastSwitchCheck)
            {
                lastSwitchCheck = trackTraverse.pieceIndex;
                string dir = bot.trackInfo.PreferredSwitch(trackTraverse.pieceIndex, trackTraverse.endLane);
                if (!string.IsNullOrEmpty(dir)) trackTraverse.Switch(dir);
            }

            TrackPiece tp = bot.trackInfo.raceTrack.pieces[trackTraverse.pieceIndex];

            if (tp.radius > 0)
            {
                double direction = 1;
                double nDrift, nAngSpeed;
                double radius = bot.trackInfo.GetLaneRadius(trackTraverse.pieceIndex, trackTraverse.pos, trackTraverse.startLane, trackTraverse.endLane);
                
                if (tp.angle < 0) direction = -direction;
                nDrift = bot.physicsModel.NextDriftAngle(drift, angSpeed, speed, radius, out nAngSpeed, direction);
                drift = nDrift;
                angSpeed = nAngSpeed;
            }
            else
            {
                double nDrift, nAngSpeed;
                nDrift = bot.physicsModel.NextDriftAngle(drift, angSpeed, speed, 0, out nAngSpeed, 1);
                drift = nDrift;
                angSpeed = nAngSpeed;
            }

            if ((turbo != null) && (turbo.turboDurationTicks >= -1)) //?? toliko traje
            {
                throttle *= turbo.turboFactor;
                turbo.turboDurationTicks--;
            }

            speed = bot.physicsModel.NextSpeed(speed, throttle);
            trackTraverse.Move(speed);
        }
    }
}
